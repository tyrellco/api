<?php

// $token_file =__DIR__."/../var/access_token";
// $fh = fopen($token_file, "r");
// $token = fgets($fh);
// fclose($fh);

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        // MySQL settings
        "db" => [
            "host" => "localhost",
            "dbname" => "manager",
            "user" => "manager", //managerweb
            "pass" => "manager", //manager09
        ],

        "api" => [
          "authorization" => "riobravo1959",
          "LFPGap" => 2
        ],
        // api fantasymarca settings
        "fantasyapi" => [
            //"authorization" => "Bearer $token",
            "url_api_player" => "https://api.laligafantasymarca.com/api/v3/player/",
            "stp_curlopt_returntransfer" => true,
            "stp_curlopt_ssl_verifypeer" => 0,
            "stp_curlopt_ssl_verifyhost" => 0,
        ],
    ],
];
