<?php
require __DIR__."/../lib/fantasymarca.inc.php";
require __DIR__."/../lib/global.inc.php";



$app->get('/admin/filepoints/{week:\d+}', function ($request, $response, $args) {
  generate_filepoints($args['week'],$this);
  $filename="jornada-".$args['week'].".csv";
  return $response
          ->withHeader('Content-type', 'text/plain')
          ->withHeader('Content-Disposition','attachment; filename="'.$filename.'"');

});

/**
*  @api {get} /admin/updateteam/:teamcode/:week Update LFP team stats
*  @apiGroup Admin
*  @apiHeader {String} X-Authorization="Token ******"
*  @apiName UpdateTeamStats
*  @apiDescription Update the stats about a team identified by TEAMCODE at the week specified by WEEK.
*  @apiParam {String} teamcode Team code of the LFP team
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/admin/updateteam/BAR/2
*      @apiSuccess (200 OK) {Array} players Array of players belonging to that LFP team.
*      @apiSuccessExample Success-Response
*   [
*    {
*      "id": "1529",
*      "idfantasy": "23",
*      "nombre": "Messi",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "90",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "1",
*      "atg_match_points": "4",
*      "atg_points": "5",
*      "atg_codeteam": "BAR"
*    },
*    {
*      "id": "1531",
*      "idfantasy": "2230",
*      "nombre": "Munir",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "1",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "0",
*      "atg_match_points": "4",
*      "atg_points": "4",
*      "atg_codeteam": "BAR"
*    },
*                   .
*                   .    
*                   .
*                   .    
*     {
*      "id": "1533",
*      "idfantasy": "2654",
*      "nombre": "Luis Suárez",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "89",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "0",
*      "atg_match_points": "4",
*      "atg_points": "4",
*      "atg_codeteam": "BAR"
*    }
*  ]
*/     

$app->get('/admin/updateteam/{codeteam}/{week}', function ($request, $response, $args) {

$header = $request->getHeaderLine('X-AUTHORIZATION');
if ($header != $this->api["authorization"]){
 $body = $response->getBody();
 $body->write('<h2>401 Unauthorized, this request requires some kind of authentication</h2>');
 return $response
       ->withStatus(401)
       ->withBody($body);
  }

$sql="SELECT localcode,guestcode FROM 0_calendariolfp WHERE jornada=:fixture AND (localcode=:codeteam OR guestcode=:codeteam)";
$sth = $this->db->prepare($sql);
$sth->bindParam("codeteam", $args['codeteam']);
$sth->bindParam("fixture", $args['week']);
$sth->execute();
$team=$sth->fetchObject();
$opponent=($team->localcode==$args['codeteam'])?$team->guestcode:$team->localcode;

//we update stats of my team and its opponent in order to prevent fails on update antiliga point process
$sql="SELECT idfantasy,equipolfp FROM 0_fantasyplayers WHERE (equipolfp=:codeteam OR equipolfp=:opponent)";
$sth = $this->db->prepare($sql);
$sth->bindParam("codeteam", $args['codeteam']);
$sth->bindParam("opponent", $opponent);
$sth->execute();
$vplayers=$sth->fetchAll();
$arrayoutput = array();
foreach ($vplayers as $key => $value) {
  $sql="SELECT id FROM 0_fantasystats WHERE idfantasy=:idplayer AND jornadalfp=:week";
  $sth = $this->db->prepare($sql);
  $sth->bindParam("idplayer", $value['idfantasy']);
  $sth->bindParam("week", $args['week']);
  $sth->execute();
  $vfantasy=$sth->fetchAll();
  if (! isset ($vfantasy[0]['id'])){ //this stat there isn't in DB, it will updated from fantasy api
    usleep(500);
    $vfantasy=get_api_player_stats($value['idfantasy'],$args['week'],$this); //get stats from fantasyapi
    update_player_stats($value['idfantasy'],$args['week'],$vfantasy,$this); // save stats into antiliga db
  }
}

get_antiliga_stats ($args['codeteam'],$args['week'],$opponent,$this);
get_antiliga_stats ($opponent,$args['week'],$args['codeteam'],$this);
$sql=<<<eof
SELECT players.nombre, stats.*  FROM 0_fantasystats stats INNER JOIN 0_fantasyplayers players ON (stats.idfantasy=players.idfantasy) WHERE stats.atg_codeteam=:codeteam AND jornadalfp=:week;
eof;
$sth = $this->db->prepare($sql);
$sth->bindParam("codeteam", $args['codeteam']);
$sth->bindParam("week", $args['week']);
$sth->execute();
$vfantasy=$sth->fetchAll();

set_last_update();

return $this->response->withJson($vfantasy, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);

});


/**
*  @api {get} /admin/resetteam/:teamcode/:week Force update the LFP team stats
*  @apiGroup Admin
*  @apiHeader {String} X-Authorization="Token ******"
*  @apiName ForceUpdateTeamStats
*  @apiDescription Force update stats about a team identified by TEAMCODE at the week specified by WEEK.
*  @apiParam {String} teamcode Team code of the LFP team
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/admin/resetteam/BAR/2
*      @apiSuccess (200 OK) {Array} players Array of players belonging to that LFP team.
*      @apiSuccessExample Success-Response
*   [
*    {
*      "id": "1529",
*      "idfantasy": "23",
*      "nombre": "Messi",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "90",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "1",
*      "atg_match_points": "4",
*      "atg_points": "5",
*      "atg_codeteam": "BAR"
*    },
*    {
*      "id": "1531",
*      "idfantasy": "2230",
*      "nombre": "Munir",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "1",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "0",
*      "atg_match_points": "4",
*      "atg_points": "4",
*      "atg_codeteam": "BAR"
*    },
*                   .
*                   .    
*                   .
*                   .    
*     {
*      "id": "1533",
*      "idfantasy": "2654",
*      "nombre": "Luis Suárez",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "89",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "0",
*      "atg_match_points": "4",
*      "atg_points": "4",
*      "atg_codeteam": "BAR"
*    }
*  ]
*/     

$app->get('/admin/resetteam/{codeteam}/{week}', function ($request, $response, $args) {

$header = $request->getHeaderLine('X-AUTHORIZATION');
if ($header != $this->api["authorization"]){
 $body = $response->getBody();
 $body->write('<h2>401 Unauthorized, this request requires some kind of authentication</h2>');
 return $response
       ->withStatus(401)
       ->withBody($body);
  }

$sql="SELECT localcode,guestcode FROM 0_calendariolfp WHERE jornada=:fixture AND (localcode=:codeteam OR guestcode=:codeteam)";
$sth = $this->db->prepare($sql);
$sth->bindParam("codeteam", $args['codeteam']);
$sth->bindParam("fixture", $args['week']);
$sth->execute();
$team=$sth->fetchObject();
$opponent=($team->localcode==$args['codeteam'])?$team->guestcode:$team->localcode;

//we update stats of my team and its opponent in order to prevent fails on update antiliga point process
$sql="SELECT idfantasy,equipolfp FROM 0_fantasyplayers WHERE (equipolfp=:codeteam OR equipolfp=:opponent)";
$sth = $this->db->prepare($sql);
$sth->bindParam("codeteam", $args['codeteam']);
$sth->bindParam("opponent", $opponent);
$sth->execute();
$vplayers=$sth->fetchAll();
$arrayoutput = array();
foreach ($vplayers as $key => $value) {
  usleep(500);
  $vfantasy=get_api_player_stats($value['idfantasy'],$args['week'],$this); //get stats from fantasyapi
  update_player_stats($value['idfantasy'],$args['week'],$vfantasy,$this); // save stats into antiliga db
}

get_antiliga_stats ($args['codeteam'],$args['week'],$opponent,$this);
get_antiliga_stats ($opponent,$args['week'],$args['codeteam'],$this);
$sql=<<<eof
SELECT players.nombre, stats.*  FROM 0_fantasystats stats INNER JOIN 0_fantasyplayers players ON (stats.idfantasy=players.idfantasy) WHERE stats.atg_codeteam=:codeteam AND jornadalfp=:week;
eof;
$sth = $this->db->prepare($sql);
$sth->bindParam("codeteam", $args['codeteam']);
$sth->bindParam("week", $args['week']);
$sth->execute();
$vfantasy=$sth->fetchAll();

set_last_update();

return $this->response->withJson($vfantasy, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);

});

/**
*  @api {get} /admin/updateplayer/:playerid/:week Update the LFP stats of a player
*  @apiGroup Admin
*  @apiHeader {String} X-Authorization="Token ******"
*  @apiName UpdatePlayerStats
*  @apiDescription Update the stats a player identified by PLAYERID at the LFP week specified by WEEK.
*  @apiParam {Number} playerid Player unique identifier
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/admin/updateplayer/96/2
*      @apiSuccess (200 OK) {String} nombre Player name
*      @apiSuccess (200 OK) {Number} id Player unique identifier (not used)
*      @apiSuccess (200 OK) {Number} idfantasy Player unique identifier of FM
*      @apiSuccess (200 OK) {Number} jornadalfp Number of the LFP week
*      @apiSuccess (200 OK) {Number} jornadal Number of the Antiliga week
*      @apiSuccess (200 OK) {Number} mins_played Number of minutes played by player selected.
*      @apiSuccess (200 OK) {Number} goals_conceded Number of goals scored by the player selected.
*      @apiSuccess (200 OK) {Number} penalty_failed Number of penalties failed by the player.
*      @apiSuccess (200 OK) {Number} penalty_save Number of penalties saved by the player selected.
*      @apiSuccess (200 OK) {Number} own_goals Number of own goals scored by the player selected.
*      @apiSuccess (200 OK) {Number} red_card Amount of red cards shown to the player.
*      @apiSuccess (200 OK) {Number} marca_points Amount of Marca points scored by the player.
*      @apiSuccess (200 OK) {String} atg_codeteam Team code of the LFP team
*      @apiSuccess (200 OK) {Number} atg_match_points Points of Antiliga match scored by the player.
*      @apiSuccess (200 OK) {Number} atg_points Total points of Antiliga scored by the player at the specified week.
*      @apiSuccessExample Success-Response
*   {
*       "nombre": "Charles",
*       "id": "6",
*       "idfantasy": "787",
*       "jornadalfp": "3",
*       "jornada": "2",
*       "mins_played": "17",
*       "goals": "1",
*       "penalty_save": "0",
*       "penalty_failed": "0",
*       "own_goals": "0",
*       "goals_conceded": "0",
*       "red_card": "0",
*       "marca_points": "3",
*       "atg_codeteam": "EIB",
*       "atg_match_points": "0",
*       "atg_points": "0",
*       "done": "0"
*   }
*/     


$app->get('/admin/updateplayer/{idplayer}/{week}', function ($request, $response, $args) {

$header = $request->getHeaderLine('X-AUTHORIZATION');
if ($header != $this->api["authorization"]){
 $body = $response->getBody();
 $body->write('<h2>401 Unauthorized, this request requires some kind of authentication</h2>');
 return $response
       ->withStatus(401)
       ->withBody($body);
  }

$sql=<<<eof
SELECT players.nombre, stats.*  FROM 0_fantasystats stats INNER JOIN 0_fantasyplayers players ON (stats.idfantasy=players.idfantasy) WHERE stats.idfantasy=:idplayer AND jornadalfp=:week;
eof;
$sth = $this->db->prepare($sql);
$sth->bindParam("idplayer", $args['idplayer']);
$sth->bindParam("week", $args['week']);
$sth->execute();
$vfantasy=$sth->fetchAll();
if (! isset ($vfantasy[0]['id'])){ //this stat there isn't in DB, it will updated from fantasy api
  $vfantasy=get_api_player_stats($args['idplayer'],$args['week'],$this);
  update_player_stats($args['idplayer'],$args['week'],$vfantasy,$this);
  $sth->execute();
  $vfantasy=$sth->fetchAll();
}

set_last_update();

return $this->response->withJson($vfantasy, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);

});

/**
*  @api {get} /admin/resetplayer/:playerid/:week Force update the LFP stats of a player
*  @apiGroup Admin
*  @apiHeader {String} X-Authorization="Token ******"
*  @apiName ForceUpdatePlayerStats
*  @apiDescription Force update the stats a player identified by PLAYERID at the LFP week specified by WEEK.
*  @apiParam {Number} playerid Player unique identifier
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/admin/resetplayer/96/2
*      @apiSuccess (200 OK) {String} nombre Player name
*      @apiSuccess (200 OK) {Number} id Player unique identifier (not used)
*      @apiSuccess (200 OK) {Number} idfantasy Player unique identifier of FM
*      @apiSuccess (200 OK) {Number} jornadalfp Number of the LFP week
*      @apiSuccess (200 OK) {Number} jornadal Number of the Antiliga week
*      @apiSuccess (200 OK) {Number} mins_played Number of minutes played by player selected.
*      @apiSuccess (200 OK) {Number} goals_conceded Number of goals scored by the player selected.
*      @apiSuccess (200 OK) {Number} penalty_failed Number of penalties failed by the player.
*      @apiSuccess (200 OK) {Number} penalty_save Number of penalties saved by the player selected.
*      @apiSuccess (200 OK) {Number} own_goals Number of own goals scored by the player selected.
*      @apiSuccess (200 OK) {Number} red_card Amount of red cards shown to the player.
*      @apiSuccess (200 OK) {Number} marca_points Amount of Marca points scored by the player.
*      @apiSuccess (200 OK) {String} atg_codeteam Team code of the LFP team
*      @apiSuccess (200 OK) {Number} atg_match_points Points of Antiliga match scored by the player.
*      @apiSuccess (200 OK) {Number} atg_points Total points of Antiliga scored by the player at the specified week.
*      @apiSuccessExample Success-Response
*   {
*       "nombre": "Charles",
*       "id": "6",
*       "idfantasy": "787",
*       "jornadalfp": "3",
*       "jornada": "2",
*       "mins_played": "17",
*       "goals": "1",
*       "penalty_save": "0",
*       "penalty_failed": "0",
*       "own_goals": "0",
*       "goals_conceded": "0",
*       "red_card": "0",
*       "marca_points": "3",
*       "atg_codeteam": "EIB",
*       "atg_match_points": "0",
*       "atg_points": "0",
*       "done": "0"
*   }
*/     

$app->get('/admin/resetplayer/{idplayer}/{week}', function ($request, $response, $args) {

$header = $request->getHeaderLine('X-AUTHORIZATION');
if ($header != $this->api["authorization"]){
 $body = $response->getBody();
 $body->write('<h2>401 Unauthorized, this request requires some kind of authentication</h2>');
 return $response
       ->withStatus(401)
       ->withBody($body);
  }

$vfantasy=get_api_player_stats($args['idplayer'],$args['week'],$this);
update_player_stats($args['idplayer'],$args['week'],$vfantasy,$this);
$sql="SELECT * FROM 0_fantasystats WHERE idfantasy=:idplayer AND jornadalfp=:week";
$sth = $this->db->prepare($sql);
$sth->bindParam("idplayer", $args['idplayer']);
$sth->bindParam("week", $args['week']);
$sth->execute();
$vfantasy=$sth->fetchAll();

set_last_update();

return $this->response->withJson($vfantasy, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);

});
