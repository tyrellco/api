<?php
//require __DIR__."/../lib/global.inc.php";

/**
*  @api {get} /stats/team/:teamcode/:week Get LFP team stats
*  @apiGroup Stats
*  @apiPermission none
*  @apiName GetTeamStats
*  @apiDescription Get stats about a team identified by TEAMCODE at the week specified by WEEK.
*  @apiParam {String} teamcode Team code of the LFP team.
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/stats/team/BAR/2
*      @apiSuccess (200 OK) {Array} players Array of players belonging to that LFP team.
*      @apiSuccessExample Success-Response
*   [
*    {
*      "id": "1529",
*      "idfantasy": "23",
*      "nombre": "Messi",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "90",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "1",
*      "atg_match_points": "4",
*      "atg_points": "5",
*      "atg_codeteam": "BAR"
*    },
*    {
*      "id": "1531",
*      "idfantasy": "2230",
*      "nombre": "Munir",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "1",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "0",
*      "atg_match_points": "4",
*      "atg_points": "4",
*      "atg_codeteam": "BAR"
*    },
*                   .
*                   .    
*                   .
*                   .    
*    {
*      "id": "1533",
*      "idfantasy": "2654",
*      "nombre": "Luis Suárez",
*      "jornada": "1",
*      "jornadalfp": "2",
*      "mins_played": "89",
*      "goals": "0",
*      "penalty_save": "0",
*      "penalty_failed": "0",
*      "own_goals": "0",
*      "goals_conceded": "0",
*      "red_card": "0",
*      "marca_points": "0",
*      "atg_match_points": "4",
*      "atg_points": "4",
*      "atg_codeteam": "BAR"
*    }
*  ]
*/     

$app->get('/stats/team/{teamcode}/{week}', function ($request, $response, $args) {

$sql=<<<eof
SELECT stats.id,stats.idfantasy,players.nombre, stats.jornada, stats.jornadalfp, stats.mins_played, stats.goals, stats.penalty_save,stats.penalty_failed, stats.own_goals,
stats.goals_conceded, stats.red_card, stats.marca_points, stats.atg_match_points,stats.atg_points,stats.atg_codeteam FROM 0_fantasystats stats INNER JOIN 0_fantasyplayers players
ON (players.idfantasy=stats.idfantasy) WHERE (stats.jornadalfp=:week and stats.atg_codeteam=:teamcode);
eof;
  $sth = $this->db->prepare($sql);
  $sth->bindParam("week", $args['week']);
  $sth->bindParam("teamcode", $args['teamcode']);
  $sth->execute();
  $arrayteam = $sth->fetchAll();
  return $this->response->withJson($arrayteam, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);

});

/**
*  @api {get} /stats/player/:playerid/:week Get Fantasy Marca stats of a player
*  @apiGroup Stats
*  @apiPermission none
*  @apiName GetPlayerStats
*  @apiDescription Get Fantasy Marca (FM) stats of a player identified by PLAYERID at the week specified by WEEK.
*  @apiParam {Number} playerid Player unique identifier.
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/stats/player/23/3
*      @apiSuccess (200 OK) {Number} idfantasy Player unique identifier of FM
*      @apiSuccess (200 OK) {String} nombre Player name
*      @apiSuccess (200 OK) {Number} mins_played Number of minutes played by player selected.
*      @apiSuccess (200 OK) {Number} goals_conceded Number of goals scored by the player selected.
*      @apiSuccess (200 OK) {Number} penalty_failed Number of penalties failed by the player.
*      @apiSuccess (200 OK) {Number} penalty_save Number of penalties saved by the player selected.
*      @apiSuccess (200 OK) {Number} own_goals Number of own goals scored by the player selected.
*      @apiSuccess (200 OK) {Number} red_card Amount of red cards shown to the player.
*      @apiSuccess (200 OK) {Number} marca_points Amount of Marca points scored by the player.
*      @apiSuccess (200 OK) {Number} atg_match_points Points of Antiliga match scored by the player.
*      @apiSuccess (200 OK) {Number} atg_points Total points of Antiliga scored by the player at the specified week.
*      @apiSuccessExample Success-Response
*  {
*    "idfantasy": "23",
*    "nombre": "Messi",
*    "mins_played": "90",
*    "goals_conceded": "2",
*    "penalty_failed": "0",
*    "penalty_save": "0",
*    "own_goals": "0",
*    "red_card": "0",
*    "marca_points": "3",
*    "atg_match_points": "4",
*    "atg_points": "17",
*  }
*/     

$app->get('/stats/player/{playerid:\d+}[/{week:\d+}]', function ($request, $response, $args) {

  if (! isset ($args["week"]))
    $args["week"] = get_current_matchday($this); //we will always show the previous matchday of the current matchady


  $sql=<<<eof
  SELECT stats.idfantasy, players.nombre, players.demarcacion, players.equipolfp, stats.mins_played, stats.goals, stats.goals_conceded, stats.penalty_failed, stats.penalty_save, stats.own_goals,
  stats.goals, stats.goals_conceded, stats.red_card, stats.marca_points, stats.atg_match_points, stats.atg_points, stats.done
  FROM 0_fantasystats stats INNER JOIN 0_fantasyplayers players ON (stats.idfantasy=players.idfantasy)
  WHERE (stats.jornadalfp=:week AND stats.idfantasy=:playerid)
eof;

    $sth = $this->db->prepare($sql);
    $sth->bindParam("playerid", $args['playerid']);
    $sth->bindParam("week", $args['week']);
    $sth->execute();

    $stats = array();
    if ($sth->rowCount() > 0)
      $stats = $sth->fetch();
    
     $stats["matchday"] =$args["week"];
      
    return $this->response->withJson($stats, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
  
  });

/**
*  @api {get} /stats/scores/:week Get scores
*  @apiGroup Stats
*  @apiPermission none
*  @apiName GetScores
*  @apiDescription Get Antiliga matches scores at the week specified by WEEK.
*  @apiParam {Array} playerid Array of players stats
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/stats/scores/3
*      @apiSuccess (200 OK) {Array} Array of players stats
*      @apiSuccess (200 OK) {String} equipo Antiliga team name
*      @apiSuccess (200 OK) {Number} puntos Points scored by the Antiliga team.
*      @apiSuccessExample Success-Response
* "10": {
*     "0": {
*       "idfantasy": "3",
*       "nombre": "Oier",
*       "mins_played": "90",
*       "goals": "0",
*       "goals_conceded": "2",
*       "penalty_failed": "0",
*       "penalty_save": "0",
*       "own_goals": "0",
*       "red_card": "0",
*       "marca_points": "1",
*       "atg_match_points": "0",
*       "atg_points": "-1",
*       "done": "1",
*       "count": 1
*     },
*     "1": {
*       "idfantasy": "5",
*       "nombre": "Piqué",
*       "mins_played": "90",
*       "goals": "0",
*       "goals_conceded": "0",
*       "penalty_failed": "0",
*       "penalty_save": "0",
*       "own_goals": "0",
*       "red_card": "0",
*       "marca_points": "2",
*       "atg_match_points": "4",
*       "atg_points": "9",
*       "done": "1",
*       "count": 2
*     }
*     .
*     .
*     .
*     "10": {
*       "idfantasy": "2510",
*       "nombre": "Ángel Correa",
*       "mins_played": "66",
*       "goals": "0",
*       "goals_conceded": "0",
*       "penalty_failed": "0",
*       "penalty_save": "0",
*       "own_goals": "0",
*       "red_card": "0",
*       "marca_points": "1",
*       "atg_match_points": "4",
*       "atg_points": "5",
*       "done": "1",
*       "count": 11
*     },
*     "equipo": "Monaguillos U.D",
*     "puntos": 46,
*     "done": 11
*   }
*/     

  
//scores to show in stumpy
$app->get('/stats/scores[/{week:\d+}]', function ($request, $response, $args) {

$lastupdate=get_last_update();

if (! isset ($args["week"])){ //if it doesn't exist week parameter via URL, then we will use current week
  $args["week"] = get_current_matchday($this);
  $weeknumber = $args["week"];
}
else
  $weeknumber=$args["week"];

$sql="SELECT id,nombre,equipacion1 FROM 0_equipos ORDER BY id";
$sth = $this->db->prepare($sql);
$sth->execute();
$arrayteam = $sth->fetchAll();

$sql=<<<eof
SELECT stats.idfantasy, players.nombre, players.demarcacion, players.equipolfp, stats.mins_played, stats.goals, stats.goals_conceded, stats.penalty_failed, stats.penalty_save, stats.own_goals,
stats.goals, stats.goals_conceded, stats.red_card, stats.marca_points, stats.atg_match_points, stats.atg_points, stats.done
FROM 0_fantasystats stats INNER JOIN 0_fantasyplayers players ON (stats.idfantasy=players.idfantasy)
WHERE stats.jornadalfp=:week
eof;

$sth = $this->db->prepare($sql);
$sth->bindParam("week", $args['week']);
$sth->execute();
$arraystats=$sth->fetchAll();
$arraystats_arranged = array();
$vpoints=array("1" => 0, "2" => 0, "3" => 0, "4" => 0, "5" => 0, "6" => 0, "7" => 0, "8" => 0,
  "9" => 0, "10" => 0, "11" => 0, "12" => 0, "13" => 0, "14" => 0);
$vdone=array("1" => 0, "2" => 0, "3" => 0, "4" => 0, "5" => 0, "6" => 0, "7" => 0, "8" => 0,
    "9" => 0, "10" => 0, "11" => 0, "12" => 0, "13" => 0, "14" => 0);

foreach ($arraystats as $key => $vstat) {
  $arraystats_arranged[$vstat['idfantasy']]=$vstat;
}

$sql=<<<eof
SELECT players.idfantasy, formations.idequipo FROM 0_alineaciones formations
INNER JOIN 0_fantasyplayers players ON (formations.idjugador=players.idantiliga)
WHERE formations.jornada=:week ORDER BY players.demarcacion ASC
eof;

$sth = $this->db->prepare($sql);
$args['week'] = $args['week'] - $this->api["LFPGap"];
$sth->bindParam("week", $args['week']);
$sth->execute();
$arrayformers=$sth->fetchAll();
$array_players= array();
$array_teams= array (
  "1" => array('players' => $array_players,'id'=>$arrayteam[0]['id'],'equipo'=>$arrayteam[0]['nombre'],'equipacion' => $arrayteam[0]['equipacion1'],'puntos'=>0,'done'=>0),
  "2" => array('players' => $array_players,'id'=>$arrayteam[1]['id'],'equipo'=>$arrayteam[1]['nombre'],'equipacion' => $arrayteam[1]['equipacion1'],'puntos'=>0,'done'=>0),
  "3" => array('players' => $array_players,'id'=>$arrayteam[2]['id'],'equipo'=>$arrayteam[2]['nombre'],'equipacion' => $arrayteam[2]['equipacion1'],'puntos'=>0,'done'=>0),
  "4" => array('players' => $array_players,'id'=>$arrayteam[3]['id'],'equipo'=>$arrayteam[3]['nombre'],'equipacion' => $arrayteam[3]['equipacion1'],'puntos'=>0,'done'=>0),
  "5" => array('players' => $array_players,'id'=>$arrayteam[4]['id'],'equipo'=>$arrayteam[4]['nombre'],'equipacion' => $arrayteam[4]['equipacion1'],'puntos'=>0,'done'=>0),
  "6" => array('players' => $array_players,'id'=>$arrayteam[5]['id'],'equipo'=>$arrayteam[5]['nombre'],'equipacion' => $arrayteam[5]['equipacion1'],'puntos'=>0,'done'=>0),
  "7" => array('players' => $array_players,'id'=>$arrayteam[6]['id'],'equipo'=>$arrayteam[6]['nombre'],'equipacion' => $arrayteam[6]['equipacion1'],'puntos'=>0,'done'=>0),
  "8" => array('players' => $array_players,'id'=>$arrayteam[7]['id'],'equipo'=>$arrayteam[7]['nombre'],'equipacion' => $arrayteam[7]['equipacion1'],'puntos'=>0,'done'=>0),
  "9" => array('players' => $array_players,'id'=>$arrayteam[8]['id'],'equipo'=>$arrayteam[8]['nombre'],'equipacion' => $arrayteam[8]['equipacion1'],'puntos'=>0,'done'=>0),
  "10" => array('players' => $array_players,'id'=>$arrayteam[9]['id'],'equipo'=>$arrayteam[9]['nombre'],'equipacion' => $arrayteam[9]['equipacion1'],'puntos'=>0,'done'=>0),
  "11" => array('players' => $array_players,'id'=>$arrayteam[10]['id'],'equipo'=>$arrayteam[10]['nombre'],'equipacion' => $arrayteam[10]['equipacion1'],'puntos'=>0,'done'=>0),
  "12" => array('players' => $array_players,'id'=>$arrayteam[11]['id'],'equipo'=>$arrayteam[11]['nombre'],'equipacion' => $arrayteam[11]['equipacion1'],'puntos'=>0,'done'=>0),
  "13" => array('players' => $array_players,'id'=>$arrayteam[12]['id'],'equipo'=>$arrayteam[12]['nombre'],'equipacion' => $arrayteam[12]['equipacion1'],'puntos'=>0,'done'=>0),
  "14" => array('players' => $array_players,'id'=>$arrayteam[13]['id'],'equipo'=>$arrayteam[13]['nombre'],'equipacion' => $arrayteam[13]['equipacion1'],'puntos'=>0,'done'=>0),
);

$arrayformers_arranged = array (
  "matchday" => $weeknumber,
  "last" => "$lastupdate",
  "teams" => $array_teams,
);

$arrayformers_arranged["teams"] = $array_teams;
$arraycount = array();
foreach ($arrayformers as $key => $vformer) {
  if (isset ($arraystats_arranged[$vformer['idfantasy']])){
    if (!isset ($arraycount[$vformer['idequipo']]))
      $arraycount[$vformer['idequipo']]=1;
    else
      $arraycount[$vformer['idequipo']]++;
    
    $arraystats_arranged[$vformer['idfantasy']]['count']=$arraycount[$vformer['idequipo']];
    array_push($arrayformers_arranged["teams"][$vformer['idequipo']]["players"],$arraystats_arranged[$vformer['idfantasy']]);
    $vpoints[$vformer['idequipo']] +=$arraystats_arranged[$vformer['idfantasy']]['atg_points'];
    $vdone[$vformer['idequipo']] +=$arraystats_arranged[$vformer['idfantasy']]['done'];
  }
}

foreach ($arrayformers_arranged["teams"] as $key => $vformers) {
  if (isset ($vpoints[$key]))
    $arrayformers_arranged["teams"][$key]['puntos']=$vpoints[$key];
  if (isset ($vdone[$key]))
    $arrayformers_arranged["teams"][$key]['done']=$vdone[$key];
}
$myArray = (array) $array_teams;
return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        ->withJson($arrayformers_arranged, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);

});
