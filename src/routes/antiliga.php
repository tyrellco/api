<?php
// Routes

/**
*  @api {get} /antiliga/teams/:id Get a team
*  @apiGroup Antiliga
*  @apiPermission none 
*  @apiName GetTeam
*  @apiDescription Get details about the team identified by ID.
*  @apiParam {Number} id Team unique ID.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/antiliga/teams/10
*      @apiSuccess (200 OK) {Number} id Team unique ID.
*      @apiSuccess (200 OK) {String} manager1 The main manager of the team
*      @apiSuccess (200 OK) {String} manager2 The second manager of the team
*      @apiSuccess (200 OK) {String} nombre Name of the team
*      @apiSuccess (200 OK) {String} estadio The stadium name of the team.
*      @apiSuccess (200 OK) {String} equipacion1 Pic file of the team shirt.
*      @apiSuccess (200 OK) {String} equipacion2 Not used.
*      @apiSuccess (200 OK) {Number} campeon Number of championship won by the team.
*      @apiSuccess (200 OK) {Number} copa Number of Antiliga Cup won by the team.
*      @apiSuccess (200 OK) {Number} cuchara Number wooden spoons won by the team.
*      @apiSuccess (200 OK) {Array} players Array of players belonging to this team.
*
*      @apiSuccessExample Success-Response
*       {
*         "id": "10",
*         "manager1": "Mario",
*         "manager2": "El Heredero",
*         "nombre": "Monaguillos U.D",
*         "estadio": "Sumo PontÃ­fice",
*         "equipacion1": "1348.png",
*         "equipacion2": "",
*         "campeon": "2",
*         "copa": "0",
*         "cuchara": "1",
*         "players": [
*           {
*             "id": "3",
*             "idprop": "10",
*             "nombre": "Oier",
*             "demarcacion": "1",
*             "goles": "0",
*             "rojas": "0",
*             "titular": "1",
*             "coste": "0",
*             "puntos": "1",
*             "jugados": "1",
*             "equipolfp": "LEV"
*           },
*           {
*             "id": "2694",
*             "idprop": "10",
*             "nombre": "Serantes",
*             "demarcacion": "1",
*             "goles": "0",
*             "rojas": "0",
*             "titular": "0",
*             "coste": "0",
*             "puntos": "0",
*             "jugados": "0",
*             "equipolfp": "LEG"
*           },
*                   .
*                   .    
*                   .
*                   .    
*          ]
*         }
*/     

$app->get('/antiliga/teams[/{teamid:\d+}]', function ($request, $response, $args) {
  if (isset($args['teamid'])){
    $sql=<<<eof
    SELECT e.*, c.general, c.regularidad, c.vuelta1, c.vuelta2, c.vuelta3
    FROM 0_equipos e INNER JOIN 0_clasificacion c ON (e.id=c.idequipo) WHERE e.id=:teamid;
eof;
    $sth = $this->db->prepare($sql);
    $sth->bindParam("teamid", $args['teamid']);
    $sth->execute();
    $arrayTeam = $sth->fetchAll();
    $sql="SELECT * from 0_jugadores WHERE idprop=:teamid";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("teamid", $args['teamid']);
    $sth->execute();
    $arrayPlayers = $sth->fetchAll();
    $arrayTeam[0]['players']=$arrayPlayers;
  }
  else {
    $sql=<<<eof
    SELECT e.*, c.general, c.regularidad, c.vuelta1, c.vuelta2, c.vuelta3
    FROM 0_equipos e INNER JOIN 0_clasificacion c ON (e.id=c.idequipo) ORDER BY e.nombre;
eof;
    $sth = $this->db->prepare($sql);
    $sth->execute();
    $arrayTeam = $sth->fetchAll();
  }
  return $response->withJson($arrayTeam, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
});

/**
*  @api {get} /antiliga/players/:teamid List of players
*  @apiGroup Antiliga
*  @apiPermission none 
*  @apiName GetPlayers
*  @apiDescription Get a list of players from the team identified by TEAMID.
*  @apiParam {Number} teamid Team unique ID.
*
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/antiliga/players/10
*      @apiSuccess (200 OK) {Array} players Array of players belonging to this team.
*
*      @apiSuccessExample Success-Response
*       [
*           {
*             "id": "3",
*             "idprop": "10",
*             "nombre": "Oier",
*             "demarcacion": "1",
*             "goles": "0",
*             "rojas": "0",
*             "titular": "1",
*             "coste": "0",
*             "puntos": "1",
*             "jugados": "1",
*             "equipolfp": "LEV"
*           },
*           {
*             "id": "2694",
*             "idprop": "10",
*             "nombre": "Serantes",
*             "demarcacion": "1",
*             "goles": "0",
*             "rojas": "0",
*             "titular": "0",
*             "coste": "0",
*             "puntos": "0",
*             "jugados": "0",
*             "equipolfp": "LEG"
*           }
*                   .
*                   .    
*                   .
*                   .    
*       ]
*/     

$app->get('/antiliga/players[/{teamid:\d+}]', function ($request, $response, $args) {
  if (isset($args['teamid'])){
    $sql="SELECT * from 0_jugadores WHERE idprop=:teamid ORDER BY demarcacion ASC, nombre";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("teamid", $args['teamid']);
  }
  else {
    $sql="SELECT * from 0_jugadores ORDER BY demarcacion ASC, nombre";
    $sth = $this->db->prepare($sql);
  }

  $sth->execute();
  $arrayPlayers = $sth->fetchAll();
  return $this->response->withJson($arrayPlayers, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
});

/**
*  @api {get} /antiliga/player/:id Get a player
*  @apiGroup Antiliga
*  @apiPermission none 
*  @apiName GetPlayer
*  @apiDescription Get info about the player identified by ID.
*  @apiParam {Number} id Player unique ID.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/player/95
*      @apiSuccess (200 OK) {Number} id Player unique ID.
*      @apiSuccess (200 OK) {Number} idprop Team owner unique ID.
*      @apiSuccess (200 OK) {String} nombre Player name.
*      @apiSuccess (200 OK) {Number} demarcacion Position id.
*      @apiSuccess (200 OK) {Number} goles Number of goals scored by the player.
*      @apiSuccess (200 OK) {Number} rojas Amount of red cards shown to the player.
*      @apiSuccess (200 OK) {Number} titular Times that this player has been in the starting lineup.
*      @apiSuccess (200 OK) {Number} coste Player price in the last auction.
*      @apiSuccess (200 OK) {Number} puntos Amount of points scored by the player.
*      @apiSuccess (200 OK) {Number} jugados Times that this player has been in the starting lineup.
*      @apiSuccess (200 OK) {String} equipolfp Identifier of the LFP's team which the player belongs.
*
*      @apiSuccessExample Success-Response
*       [
*       {
*         "id": "95",
*         "idprop": "13",
*         "nombre": "S. Asenjo ",
*         "demarcacion": "1",
*         "goles": "0",
*         "rojas": "0",
*         "titular": "1",
*         "coste": "0",
*         "puntos": "9",
*         "jugados": "1",
*         "equipolfp": "VIL"
*       }
*      ]
*/   

$app->get('/antiliga/player/{playerid:\d+}', function ($request, $response, $args) {

  $sql=<<<eof
SELECT j.id, j.idprop, j.nombre, j.demarcacion, j.goles, j.rojas, j.titular, j.coste, 
j.puntos, j.jugados, j.equipolfp, lfp.nombre lfpteam, e.nombre equipo from 0_jugadores j INNER JOIN 0_equipos e 
ON (j.idprop = e.id ) INNER JOIN 0_equiposlfp lfp ON (lfp.codigo = j.equipolfp) WHERE j.id=:playerid
eof;

  $sth = $this->db->prepare($sql);
  $sth->bindParam("playerid", $args['playerid']);
  $sth->execute();
  $arrayPlayer = $sth->fetch();

  return $this->response->withJson($arrayPlayer, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
});

/**
*  @api {get} /antiliga/formations/:teamid/:week Get starting lineup
*  @apiGroup Antiliga
*  @apiPermission none 
*  @apiName GetFormations
*  @apiDescription Get the starting lineup of the team identified by TEAMID at the week specified by WEEK.
*  @apiParam {Number} teamid Team unique ID.
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/antiliga/formations/10/2
*      @apiSuccess (200 OK) {Array} Array of players belonging to this team.

*      @apiSuccessExample Success-Response
* [
*   {
*     "jornada": 37,
*     "idjugador": 5,
*     "nombre": "Piqué",
*     "demarcacion": 2,
*     "idequipo": 10,
*     "fecha": "sáb, 18 may 2019 10:57:24 CEST",
*     "puntos": 4,
*     "roja": 0,
*     "gol": 0,
*     "jugado": 1
*   },
*   {
*     "jornada": 37,
*     "idjugador": 23,
*     "nombre": "Messi",
*     "demarcacion": 4,
*     "idequipo": 10,
*     "fecha": "sáb, 18 may 2019 10:57:24 CEST",
*     "puntos": 14,
*     "roja": 0,
*     "gol": 0,
*     "jugado": 1
*   },
*   .
*   .
*   .
*   {
*     "jornada": 37,
*     "idjugador": 717,
*     "nombre": "Pere Pons",
*     "demarcacion": 3,
*     "idequipo": 10,
*     "fecha": "sáb, 18 may 2019 10:57:24 CEST",
*     "puntos": 1,
*     "roja": 0,
*     "gol": 0,
*     "jugado": 1
*   }
* [
*/   

$app->get('/antiliga/formations/{teamid}/{week}', function ($request, $response, $args) {
  $sql=<<<eof
SELECT formations.jornada, formations.idjugador, players.nombre, players.demarcacion,
formations.idequipo, formations.fecha, formations.puntos, formations.roja,
formations.gol, formations.jugado FROM 0_alineaciones formations
INNER JOIN 0_jugadores players ON (players.id=formations.idjugador)
WHERE (formations.idequipo=:teamid AND formations.jornada=:week) 
ORDER BY players.demarcacion ASC
eof;
  $sth = $this->db->prepare($sql);
  $sth->bindParam("week", $args['week']);
  $sth->bindParam("teamid", $args['teamid']);
  $sth->execute();
  $arrayFormations = $sth->fetchAll();
  return $this->response->withJson($arrayFormations, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
});

/**
*  @api {get} /antiliga/results/:week Get results
*  @apiGroup Antiliga
*  @apiPermission none 
*  @apiName GetResults
*  @apiDescription Get the results of the matches at week specified by WEEK.
*  @apiParam {Number} week Number of the week.
*  @apiExample {json} Example usage:
*      GET http://api.antiliga.org/antiliga/results/1
*      @apiSuccess (200 OK) {Number} idlocal Local team unique identifier
*      @apiSuccess (200 OK) {String} local Local team name.
*      @apiSuccess (200 OK) {Number} puntoslocal Points scored by the local team
*      @apiSuccess (200 OK) {Number} idvistante Guest team unique identifier
*      @apiSuccess (200 OK) {String} visitante Guest team name.
*      @apiSuccess (200 OK) {Number} puntosvisitante Points scored by the guest team
*
*      @apiSuccessExample Success-Response
* 
* {
*  "jornada": 10,
*  "jornadalfp": 11,
*  "fecha": "04/11/2018",
*  "vuelta": 1,
*  "jornadant": 10,
*  "matches": [
*    {
*      "idlocal": 13,
*      "local": "Morones de la Frontera",
*      "equipacionlocal": "1300.png",
*      "puntoslocal": 52,
*      "idvisitante": 8,
*      "visitante": "Los Shicos",
*      "equipacionvisitante": "1007.png",
*      "puntosvisitante": 55
*    },
*    {
*      "idlocal": 14,
*      "local": "UD La Confianza",
*      "equipacionlocal": "1146.png",
*      "puntoslocal": 41,
*      "idvisitante": 2,
*      "visitante": "Mineritos",
*      "equipacionvisitante": "1150.png",
*      "puntosvisitante": 59
*    },
*    {
*      "idlocal": 12,
*      "local": "Matoteam",
*      "equipacionlocal": "1112.png",
*      "puntoslocal": 63,
*      "idvisitante": 3,
*      "visitante": "Frikillos",
*      "equipacionvisitante": "1178.png",
*      "puntosvisitante": 39
*    },
*    {
*      "idlocal": 9,
*      "local": "Malaventura F.C.",
*      "equipacionlocal": "1242.png",
*      "puntoslocal": 43,
*      "idvisitante": 11,
*      "visitante": "Pepones F.C.",
*      "equipacionvisitante": "1011.png",
*      "puntosvisitante": 50
*    },
*    {
*      "idlocal": 4,
*      "local": "Cidanes",
*      "equipacionlocal": "1263.png",
*      "puntoslocal": 39,
*      "idvisitante": 10,
*      "visitante": "Monaguillos U.D",
*      "equipacionvisitante": "1348.png",
*      "puntosvisitante": 44
*    },
*    {
*      "idlocal": 7,
*      "local": "Tetra FC",
*      "equipacionlocal": "1252.png",
*      "puntoslocal": 29,
*      "idvisitante": 6,
*      "visitante": "Alatriste F.C.",
*      "equipacionvisitante": "1034.png",
*      "puntosvisitante": 31
*    },
*    {
*      "idlocal": 1,
*      "local": "Prenchitós",
*      "equipacionlocal": "1176.png",
*      "puntoslocal": 40,
*      "idvisitante": 5,
*      "visitante": "PapiJuli",
*      "equipacionvisitante": "1066.png",
*      "puntosvisitante": 41
*    }
*  ]
* }
*
*/   

$app->get('/antiliga/results/{week:\d+$|last$}', function ($request, $response, $args) {
  
  $arrayOutput = array();
  $arrayFixture = array();
  $arrayTeams = array();
  $arrayOutputResults = array();


  if ($args['week']==='last'){
    $sql = "SELECT * FROM `0_calendario` WHERE `fechaunix`< :today AND `mostrar`<>0 ORDER BY `numjornada` DESC LIMIT 1";
    $sth = $this->db->prepare($sql);
    $time = time();
    $sth->bindParam("today", $time);
    $sth->execute();
    $calendarObj = $sth->fetchObject();
    $args['week'] = ($calendarObj->numjornada ==0) ? 1 : $calendarObj->numjornada;
  } 

  $sql = "SELECT * FROM `0_calendario` WHERE `numjornada`=:week";
  $sth = $this->db->prepare($sql);
  $sth->bindParam("week", $args['week']);
  $sth->execute();
  
  $calendarObj = $sth->fetchObject();
  $args['week'] = $calendarObj->numjornada;
  $arrayOutput["jornada"] = $calendarObj->numjornada;
  $arrayOutput["jornadalfp"] = $calendarObj->jornadalfp;
  $arrayOutput["fecha"] = $calendarObj->fecha;
  $arrayOutput["vuelta"] = $calendarObj->vuelta;
  $arrayOutput["jornadant"] = $calendarObj->jornadant;


  $sql="SELECT idequipo1, idequipo2 from 0_enfrentamientos WHERE numjornada=:week";
  $sth = $this->db->prepare($sql);
  $sth->bindParam("week", $args['week']);
  $sth->execute();
  
  $arrayMatches = $sth->fetchAll();

  $sql = "SELECT id, nombre, equipacion1 FROM 0_equipos";
  $sth = $this->db->prepare($sql);
  $sth->execute();
  $teams = $sth->fetchAll();

  foreach ($teams as $team){
    $arrayFixture[$team["id"]]['puntos'] = 0;
    $arrayTeams[$team["id"]]["name"] = $team["nombre"];
    $arrayTeams[$team["id"]]["shirt"] = $team["equipacion1"];
  }

  $sql=<<<eof
  SELECT formations.idequipo, SUM(formations.puntos) puntos FROM 0_alineaciones formations INNER JOIN 0_equipos teams
  ON (teams.id=formations.idequipo) WHERE formations.jornada=:week GROUP BY teams.nombre;
eof;
  $sth = $this->db->prepare($sql);
  $sth->bindParam("week", $args['week']);
  $sth->execute();
  $arrayResults = $sth->fetchAll();

  foreach ($arrayResults as $key => $value) {
    $arrayFixture[$value['idequipo']]['puntos']= $arrayResults[$key]['puntos'];

  }

  foreach ($arrayMatches as $key => $value) {
    $arrayAux = array(
      'idlocal'=>$value['idequipo1'],
      'local'=>$arrayTeams[$value['idequipo1']]["name"],
      'equipacionlocal'=>$arrayTeams[$value['idequipo1']]["shirt"],
      'puntoslocal'=> $arrayFixture[$value['idequipo1']]['puntos'],
      'idvisitante'=>$value['idequipo2'],
      'visitante'=>$arrayTeams[$value['idequipo2']]["name"],
      'equipacionvisitante'=>$arrayTeams[$value['idequipo2']]["shirt"],
      'puntosvisitante'=> $arrayFixture[$value['idequipo2']]['puntos']
    );
    array_push($arrayOutputResults,$arrayAux);
  }
  $arrayOutput["matches"] = $arrayOutputResults;

  return $this->response->withJson($arrayOutput, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
});


$app->get('/antiliga/rankings', function ($request, $response, $args) {

  $arrayResults = array();
  $now=time();
  $sql="SELECT vuelta FROM 0_calendario WHERE fechaunix < :moment AND mostrar<>0 ORDER BY numjornada DESC LIMIT 1";
  $sth = $this->db->prepare($sql);
  $sth->bindParam("moment", $now);
  $sth->execute();
  $row = $sth->fetchObject();
  $currentRound = ($row->vuelta == 0) ? "vuelta1" : "vuelta".$row->vuelta;
  

  $sql=<<<eof
  SELECT e.*, c.general, c.regularidad, c.vuelta1, c.vuelta2, c.vuelta3, c.$currentRound vuelta, c.general puntos FROM 0_clasificacion c 
  INNER JOIN 0_equipos e ON (e.id=c.idequipo) ORDER by c.general DESC, c.regularidad DESC
eof;
  $sth = $this->db->prepare($sql);
  $sth->execute();
  $arrayResults["general"] = $sth->fetchAll();

  $sql=<<<eof
  SELECT e.*, c.general, c.regularidad, c.vuelta1, c.vuelta2, c.vuelta3, c.$currentRound vuelta, c.regularidad puntos FROM 0_clasificacion c 
  INNER JOIN 0_equipos e ON (e.id=c.idequipo) ORDER by c.regularidad DESC, c.general DESC
eof;
  $sth = $this->db->prepare($sql);
  $sth->execute();
  $arrayResults["regularity"] = $sth->fetchAll();

  $sql=<<<eof
  SELECT e.*, c.general, c.regularidad, c.vuelta1, c.vuelta2, c.vuelta3, c.$currentRound vuelta, c.$currentRound puntos FROM 0_clasificacion c 
  INNER JOIN 0_equipos e ON (e.id=c.idequipo) ORDER by c.$currentRound DESC, c.regularidad DESC
eof;
  $sth = $this->db->prepare($sql);
  $sth->execute();
  $arrayResults["round"] = $sth->fetchAll();
  
  return $this->response->withJson($arrayResults, null, JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
});