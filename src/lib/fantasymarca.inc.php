<?php

require __DIR__."/antiliga.inc.php";

function get_api_player_stats ($idplayer,$week,$app){

$curl_handler = curl_init();
// $headers = array(
//     "Authorization: ".$app->fantasyapi["authorization"]
// );

curl_setopt($curl_handler, CURLOPT_URL, $app->fantasyapi["url_api_player"].$idplayer);
curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, $app->fantasyapi["stp_curlopt_returntransfer"]);
curl_setopt($curl_handler, CURLOPT_SSL_VERIFYPEER, $app->fantasyapi["stp_curlopt_ssl_verifypeer"]);
curl_setopt($curl_handler, CURLOPT_SSL_VERIFYHOST, $app->fantasyapi["stp_curlopt_ssl_verifyhost"]);
//curl_setopt($curl_handler, CURLOPT_HTTPHEADER, $headers);

$jsonObj=curl_exec($curl_handler);
$phpObj=json_decode($jsonObj,true);
$vfantasy=array();
$found=0;

//looking for fixture
if (array_key_exists('playerStats', $phpObj)) {
  foreach ($phpObj["playerStats"] as $statobj) {
    if ($statobj["weekNumber"]==$week){
      $found=1;
      $vfantasy["mins_played"] = $statobj["stats"]["mins_played"][0];
      $vfantasy["goals"] = $statobj["stats"]["goals"][0];
      $vfantasy["penalty_save"] = $statobj["stats"]["penalty_save"][0];
      $vfantasy["penalty_failed"] = $statobj["stats"]["penalty_failed"][0];
      $vfantasy["own_goals"] = $statobj["stats"]["own_goals"][0];
      $vfantasy["goals_conceded"] = $statobj["stats"]["goals_conceded"][0];
      $vfantasy["red_card"] = $statobj["stats"]["red_card"][0];
      if ($vfantasy["red_card"]==0) //if there isn't red_card we'll look if there ir two yellow cards.
        $vfantasy["red_card"] = $statobj["stats"]["second_yellow_card"][0];
      $vfantasy["marca_points"] = $statobj["stats"]["marca_points"][1];
      break;
    }
  }
}

  return $vfantasy;
}

function update_player_stats($idplayer,$fixture,$vstat,$app){

  $sql="SELECT * FROM 0_fantasyplayers WHERE idfantasy=:idplayer";
  $sth = $app->db->prepare($sql);
  $sth->bindParam("idplayer", $idplayer);
  $sth->execute();
  $vfantasy=$sth->fetchAll();
  $codeteam=$vfantasy[0]['equipolfp'];
	//clean
	$sql="DELETE FROM 0_fantasystats WHERE idfantasy=:idplayer AND jornadalfp=:week";
  $sth = $app->db->prepare($sql);
  $sth->bindParam("idplayer", $idplayer);
  $sth->bindParam("week", $fixture);
  $sth->execute();

  //getting finished matches
  $sql="SELECT count(*) finished FROM 0_fantasystats WHERE (mins_played = 90 AND jornadalfp = :week AND atg_codeteam=:codeteam)";
  $sth = $app->db->prepare($sql);
  $sth->bindParam("week", $fixture);
  $sth->bindParam("codeteam", $codeteam);
  $sth->execute();
  $vfinished=$sth->fetchObject();
  $done=0;
  if ($vfinished->finished > 0)
    $done=1;

	$fields="";
	$values="";
	$fixtureant=$fixture-$app->api["LFPGap"];
	$sql="INSERT INTO 0_fantasystats ";
	if (!empty($vstat)){
		foreach ($vstat as $key => $value) {
			$fields .=$key.",";
			$values .=$value.",";
		}
		$sql .="(".$fields."jornadalfp,jornada,idfantasy,atg_codeteam,atg_match_points,atg_points,done) VALUES (".$values.$fixture.",".$fixtureant.",'".$idplayer."','".$codeteam."',0,0,".$done.")";
	}
	else{
		$sql .="(mins_played,goals,penalty_save,penalty_failed,own_goals,goals_conceded,red_card,marca_points,jornadalfp,jornada,idfantasy,atg_codeteam,atg_match_points,atg_points,done) ";
		$sql .="VALUES (0,0,0,0,0,0,0,0,".$fixture.",".$fixtureant.",'".$idplayer."','".$codeteam."',0,0,".$done.")";
	}

  $sth = $app->db->prepare($sql);
  $sth->execute();

}

//Calculate and save antiliga points of a team and his opponent
function get_antiliga_stats ($codeteam,$fixture,$opponent,$app){
	//initialize antiliga settings and points rules
	$vantiliga_scoring=get_antiliga_settings();

	//my team goals
	$sql="SELECT goals_conceded FROM 0_fantasystats WHERE mins_played=90 AND jornadalfp=:fixture AND atg_codeteam=:codeteam LIMIT 1";
  $sth = $app->db->prepare($sql);
  $sth->bindParam("codeteam", $codeteam);
  $sth->bindParam("fixture", $fixture);
  $sth->execute();
	$stat=$sth->fetchObject();
  if (!isset ($stat->goals_conceded))
    $goals_conceded_myteam=0;
  else
    $goals_conceded_myteam=$stat->goals_conceded;

	//goals of his opponent
	$sql="SELECT goals_conceded FROM 0_fantasystats WHERE mins_played=90 AND jornadalfp=:fixture AND atg_codeteam=:opponent LIMIT 1";
  $sth = $app->db->prepare($sql);
  $sth->bindParam("opponent", $opponent);
  $sth->bindParam("fixture", $fixture);
  $sth->execute();
	$stat=$sth->fetchObject();
  if (!isset ($stat->goals_conceded))
    $goals_conceded_opponent=0;
  else
    $goals_conceded_opponent=$stat->goals_conceded;

  // update points for match (victory or tie)
	if ($goals_conceded_myteam > $goals_conceded_opponent){ //codeteam lost, his opponent win
		$sql="UPDATE 0_fantasystats SET atg_match_points=0 WHERE jornadalfp=".$fixture." AND atg_codeteam='".$codeteam."'" ;
	}
	else{
		if ($goals_conceded_myteam < $goals_conceded_opponent){ //codeteam win, his opponent lost
			$sql="UPDATE 0_fantasystats SET atg_match_points=4 WHERE jornadalfp=".$fixture." AND atg_codeteam='".$codeteam."' AND mins_played > 0";
		}
		else
			//codeteam and opponent tie
      $sql="UPDATE 0_fantasystats SET atg_match_points=2 WHERE jornadalfp=".$fixture." AND atg_codeteam='".$codeteam."' AND mins_played > 0";
	}
  $sth = $app->db->prepare($sql);
  $sth->execute();

	//update antiliga points
	$sql="SELECT * FROM 0_fantasystats WHERE (jornadalfp=".$fixture." AND atg_codeteam='".$codeteam."')";
  $sth = $app->db->prepare($sql);
  $sth->execute();
  $vdbstats=$sth->fetchAll();

  foreach ($vdbstats as $key => $vfantasy) {
		if ($vfantasy["mins_played"]==0) // if player has not played any time -> 0 antiliga points
			$atg_points=0;
		else{
			$sql="SELECT demarcacion FROM 0_fantasyplayers WHERE idfantasy='".$vfantasy["idfantasy"]."'";
      $sth = $app->db->prepare($sql);
      $sth->execute();
      $player=$sth->fetchObject();

			switch ($player->demarcacion) {
			case '1':
				$atg_points=get_gkp_points($vantiliga_scoring["all"],$vantiliga_scoring["gkp"],$vfantasy);
				break;
			case '2':
				$atg_points=get_def_points($vantiliga_scoring["all"],$vantiliga_scoring["def"],$vfantasy,$goals_conceded_myteam);
				break;
			case '3':
				$atg_points=get_mid_points($vantiliga_scoring["all"],$vantiliga_scoring["mid"],$vfantasy);
				break;
			case '4':
				$atg_points=get_for_points($vantiliga_scoring["all"],$vantiliga_scoring["for"],$vfantasy);
				break;
			default:
				break;
			}
		}

		$sql="UPDATE 0_fantasystats SET atg_points=".$atg_points." WHERE jornadalfp=".$fixture." AND idfantasy='".$vfantasy["idfantasy"]."'";
    $sth = $app->db->prepare($sql);
    $sth->execute();

	}
}

function generate_filepoints($fixture,$app){
	$sql="SELECT fp.idantiliga id, fs.idfantasy idfantasy, fp.nombre nombre, fp.equipolfp lfp, fs.atg_points puntos_jornada,fs.goals goles, ";
	$sql .="fp.demarcacion demarcacion FROM `0_fantasystats` fs INNER JOIN `0_fantasyplayers` fp ON (fp.idfantasy=fs.idfantasy) WHERE fs.jornadalfp=:fixture";
  $sth = $app->db->prepare($sql);
  $sth->bindParam("fixture", $fixture);
  $sth->execute();
  $vdbstats=$sth->fetchAll();

	$output="";
  foreach ($vdbstats as $key => $vfile) {
		$sql="SELECT SUM(atg_points) total_points, SUM(goals) total_goals FROM `0_fantasystats` ";
		$sql .="WHERE `idfantasy`='".$vfile["idfantasy"]."'";
    $sth = $app->db->prepare($sql);
    $sth->execute();
    $vplayer=$sth->fetchObject();
		$total_goals=$vplayer->total_goals;
		$total_points=$vplayer->total_points;
		$output .=$vfile["id"]."#".utf8_encode($vfile["nombre"])."#".$vfile["lfp"]."#".$vfile["puntos_jornada"]."#".$total_points."#".$total_goals."#".$vfile["demarcacion"]."\n";
	}
	echo $output;
}
