<?php

function get_antiliga_settings(){
	return parse_ini_file("conf/antiliga.ini", true);
}

function get_generic_points($vgeneric,$vfantasy){
	$total=0;

	//penalties parados
	$total +=$vfantasy["penalty_save"] * $vgeneric["penalty_save"];
	//penalties fallados
	$total +=$vfantasy["penalty_failed"] * $vgeneric["penalty_failed"];
	// goles en pp
	$total +=$vfantasy["own_goals"] * $vgeneric["own_goals"];
	// rojas
	$total +=$vfantasy["red_card"] * $vgeneric["red_card"];
	//puntos marca
	$total+=$vfantasy["marca_points"];
	// puntos por victoria o empate siempre que haya jugado algo
	if ($vfantasy["mins_played"] > 0)
		$total+=$vfantasy["atg_match_points"];

	return $total;

}

function get_gkp_points($vgeneric,$vgkp,$vfantasy){
	$total=0;

	//portería imbatida
	if (($vfantasy["goals_conceded"]==0) && ($vfantasy["mins_played"]>=60))
		$total +=$vgkp["goals_conceded"];
	else{
		if ($vfantasy["goals_conceded"] > 1)
			$total -=$vfantasy["goals_conceded"];
	}

	//goles a favor
	$total +=$vfantasy["goals"] * $vgkp["goals"];
	$total +=get_generic_points($vgeneric,$vfantasy);

	return $total;

}

function get_def_points($vgeneric,$vdef,$vfantasy,$goals_conceded_myteam){
	$total=0;

	//portería imbatida
	if (($goals_conceded_myteam==0) && ($vfantasy["mins_played"]>=45))
		$total +=$vdef["goals_conceded"];

	//goles a favor
	$total +=$vfantasy["goals"] * $vdef["goals"];
	$total +=get_generic_points($vgeneric,$vfantasy);

	return $total;

}

function get_mid_points($vgeneric,$vmid,$vfantasy){
	$total=0;

	//goles a favor
	$total +=$vfantasy["goals"] * $vmid["goals"];
	$total +=get_generic_points($vgeneric,$vfantasy);

	return $total;

}

function get_for_points($vgeneric,$vfor,$vfantasy){
	$total=0;

	//goles a favor
	$total +=$vfantasy["goals"] * $vfor["goals"];
	$total +=get_generic_points($vgeneric,$vfantasy);

	return $total;

}
