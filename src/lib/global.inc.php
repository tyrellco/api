<?php


function get_current_matchday($app){
  $matchday = 38;
  $now=time();
  $sql="SELECT * from 0_calendario WHERE fechaunix > :now LIMIT 1";
  $sth = $app->db->prepare($sql);
  $sth->bindParam("now", $now);
  $sth->execute();
  if ($sth->rowCount() > 0) {
    $result = $sth->fetchAll();
    
    $dif=$result[0]["fechaunix"] - $now;
    if ($dif > 172800){ //172800 two days in seconds, if date founded is greater than (current date + 2 days) then it will be selected the previous week.
      $matchday = $result[0]["jornadalfp"]-1;
    }
    else{
      $matchday = $result[0]["jornadalfp"];
    }
  }
  
  return $matchday;
}

function set_last_update(){
  //update date info
  setlocale(LC_ALL, 'es_ES');
  $filelast =__DIR__."/../../var/update.last";
  $fh = fopen($filelast, "w");
  fwrite($fh,strftime("%c"));
  fclose($fh);
}

function get_last_update(){
  $filelast =__DIR__."/../../var/update.last";
  $fh = fopen($filelast, "r");
  $lastupdate = fgets($fh);
  fclose($fh);

  return utf8_encode($lastupdate);
}
